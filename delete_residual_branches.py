import argparse
import logging
from typing import Sequence, Iterator
import subprocess
import sys


DEFAULT_KEEP_BRANCHES = 'develop,master'


def get_branches_iterator(remote: bool) -> Iterator[str]:
    list_branches_command = ['git', 'branch', '--list']
    if remote:
        list_branches_command.append('--all')
    list_branches_command.append('--merged')

    git_branches_result = subprocess.run(
        list_branches_command,
        capture_output=True
    )
    git_branches_result.check_returncode()
    for branch in git_branches_result.stdout.decode('utf-8').splitlines():
        branch = branch.strip()
        logging.debug("Rama encontrada: %s", branch)
        if branch.startswith('*'):
            continue
        yield branch


def delete_branch(branch: str, real_delete: bool) -> None:
    if not real_delete:
        logging.info("Se borraría la rama '%s'", branch)
        return
    result = subprocess.run(['git', 'branch', '-d', branch])
    if result.returncode != 0:
        logging.warning("No se ha podido borrar la rama '%s'", branch)
    else:
        logging.info("Rama '%s' borrada", branch)


def build_argument_parser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        description="Script para borrar ramas locales ya mergeadas en la rama actual"
    )
    parser.add_argument(
        '--keep', required=False, default=DEFAULT_KEEP_BRANCHES,
        help=("Lista de ramas a mantener aunque ya estén mergeadas, separadas por comas; "
              "por defecto '{0!s}'".format(DEFAULT_KEEP_BRANCHES))
    )
    parser.add_argument(
        '--save', action='store_true',
        help="Borra de verdad las ramas; sin este argumento no borra, solo muestra lo que borraría"
    )
    parser.add_argument(
        '--remote', action='store_true',
        help="Tiene en cuenta también ramas remotas; por defecto, solo las locales"
    )
    parser.add_argument(
        '--verbose', action='store_true', help="Modo verboso"
    )
    return parser


def main(argv: Sequence[str]) -> None:
    args = build_argument_parser().parse_args(argv)
    log_level = logging.INFO
    if args.verbose:
        log_level = logging.DEBUG
    logging.basicConfig(level=log_level)
    logging.debug("Argumentos: %s", args)
    branches_to_keep = args.keep.split(",")
    for branch in get_branches_iterator(args.remote):
        if branch in branches_to_keep:
            logging.debug("Saltando rama '%s' por estar en branches_to_keep", branch)
            continue
        delete_branch(branch, args.save)


if __name__ == '__main__':
    main(sys.argv[1:])
